package my;

/**
 * Created by Android on 07.05.2017.
 */

import com.amazonaws.mobileconnectors.s3.transferutility.TransferService;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;

import java.util.HashMap;
import java.util.Map;

/**
 * A holder of S3 clients for {@link TransferUtility} to pass a reference of
 * AmazonS3 to {@link TransferService}. Usually objects are passed to a service
 * via intent in a parcelable form. A S3 client has too many elements to
 * capture. Instead, this serves as an alternative approach, not ideal though.
 */
public class S3ClientReference {

    private static Map<String, AmazonS3> map = new HashMap<String, AmazonS3>();

    public static void put(String key, AmazonS3 s3) {
        map.put(key, s3);
    }

    /**
     * Retrieves the AmazonS3 client on the given key.
     *
     * @param key key of the client
     * @return an AmazonS3 instance, or null if the key doesn't exist
     */
    public static AmazonS3 get(String key) {
        return map.remove(key);
    }

    /**
     * Clears all references.
     */
    public static void clear() {
        map.clear();
    }
}
