package my;

import android.app.Application;
import android.content.Context;

/**
 * Created by Android on 07.05.2017.
 */

public class MyApplication extends Application {
    private static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static Context getAppContext(){
        return context;
    }
}
