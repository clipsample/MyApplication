package my;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.demo.s3transferutility.Constants;
import com.amazonaws.demo.s3transferutility.UploadActivity;
import com.amazonaws.demo.s3transferutility.Util;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;

import java.io.File;


public class FileUploadIntentService extends IntentService {

    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "my.action.FOO";
    private static final String ACTION_BAZ = "my.action.BAZ";

    // TODO: Rename parameters
    private static final String VIDEO_FILE_PATH = "VIDEO_FILE_PATH";
    private static final String LEAD_ID = "LEAD_ID";
    private static final String TAG = "myLogs";

    private final String amazonAccessPath = "d1wc6cu8i70qi6.cloudfront.net";
    private final String amazonBucketName = "mobileclipcall2";
//    private final String amazonBucketRootFolder = "ClipCallConsumersVideos";
    private final String amazonBucketRootFolder = "media_qa";


    private static AmazonS3Client sS3Client;
    private static CognitoCachingCredentialsProvider sCredProvider;
    private static TransferUtility transferUtility;

    public FileUploadIntentService() {
        super("FileUploadIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        transferUtility = getTransferUtility(MyApplication.getAppContext());
    }


    private static CognitoCachingCredentialsProvider getCredProvider(Context context) {
        if (sCredProvider == null) {
            sCredProvider = new CognitoCachingCredentialsProvider(
                    context,
                    Constants.COGNITO_POOL_ID,
                    Regions.fromName(Constants.COGNITO_POOL_REGION));
        }else {
            Log.w(TAG, "getCredProvider: " );
        }
        return sCredProvider;
    }


    public static AmazonS3Client getS3Client(Context context) {
        if (sS3Client == null) {
            sS3Client = new AmazonS3Client(getCredProvider(context));
            sS3Client.setRegion(Region.getRegion(Regions.fromName(Constants.BUCKET_REGION)));
        }else {
            Log.w(TAG, "getS3Client: " );
        }
        return sS3Client;
    }

     public static TransferUtility getTransferUtility(Context context) {
        if (transferUtility == null) {
            transferUtility = new TransferUtility(getS3Client(context),
                    context);
        }else {
            Log.w(TAG, "getTransferUtility: " );
        }

        return transferUtility;
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e("myLogs", "onHandleIntent: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" );

//       Intent fuckIntent = new Intent(this, MyService.class);
//        startService(fuckIntent);

        String s3Key = intent.getStringExtra("INTENT_BUNDLE_S3_REFERENCE_KEY");
        Log.w(TAG, "onHandleIntent: s3Key =  " + s3Key );

//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    for (int i = 0; i < 10; i++) {
//                        Log.w(TAG, " new Thread run: i = " + i );
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }).start();
        if (intent != null) {
                final String videoFilePath = intent.getStringExtra(VIDEO_FILE_PATH);
                final String leadId = intent.getStringExtra(LEAD_ID);

            if (videoFilePath == null){
             return;
            }
            if (leadId == null ){
                return;
            }

//            testingNewTread();
            TransferListener listener = new UploadListener();

            File file = new File(videoFilePath);
            String ammazonFilePath = getAmazonFilePath(file.getName(), leadId);
            if (transferUtility != null) {
                 TransferObserver observer = transferUtility.upload(Constants.BUCKET_NAME, ammazonFilePath ,file, CannedAccessControlList.PublicRead);
                observer.setTransferListener(listener);
            }else {
                Log.w(TAG, "onHandleIntent: transferUtility = null !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" );
            }
        }
    }

    private String getAmazonFilePath(String localPath, String leadId){
      String result = "errorGettingFilePath";
//        testingNewTread();
        if (localPath != null && !localPath.isEmpty()){

            String filenameArray[] = localPath.split("\\.");
            String filename = filenameArray[filenameArray.length-2];
            String extension = filenameArray[filenameArray.length-1];
            if (leadId != null && !leadId.isEmpty()){
                result = amazonBucketRootFolder + "/" + leadId + "/" + filename + "/" + filename + "." + extension;
                Log.w(TAG, "getAmazonFilePath: result = " + result );
                filename = "test";

                result = amazonBucketRootFolder + "/" + "d7a39675-3c9b-419c-8a62-d7696fe9a8c4/" +  filename + "/" + filename + "9." + extension;

                result = amazonBucketRootFolder + "/d7a39675-3c9b-419c-8a62-d7696fe9a8c4/1494239172340/1494239172340.mp4";
                Log.w(TAG, "getAmazonFilePath: result = " + result );
            }
        }
        return result;
    }
    private void testingNewTread() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.e(TAG, "run: started" );
                for (int i = 0; i < 50; i++) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Log.e(TAG, "run: i = " + i);
                }
                Log.e(TAG, "run: finished");
            }
        }).start();

    }
    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String param1, String param2) {
        // TODO: Handle action Foo
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("myLogs", "onDestroy: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" );
    }
    private class UploadListener implements TransferListener {

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e(TAG, "Error during upload: " + id, e);

        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d(TAG, String.format("onProgressChanged: %d, total: %d, current: %d",
                    id, bytesTotal, bytesCurrent));

        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.d(TAG, "onStateChanged: " + id + ", " + newState);

        }
    }
}
