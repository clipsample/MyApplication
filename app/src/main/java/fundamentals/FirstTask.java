package fundamentals;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Android on 12.06.2017.
 */

public class FirstTask {
    {
        for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
//            System.out.println(ste);
//            Log.w(TAG, "sendMessage:  " + ste);
        }
    }

    static  String adr = "Address[addressLines=[0:\"Ventura Fwy, Hidden Hills, CA 91302, USA\"],feature=Ventura Freeway,admin=California,sub-admin=Los Angeles County,locality=Hidden Hills,thoroughfare=Ventura Freeway,postalCode=91302,countryCode=US,countryName=United States,hasLatitude=true,latitude=34.1566281,hasLongitude=true,longitude=-118.64490350000001,phone=null,url=null,extras=null]";
    static  String sConversationKey = "81762557-fc3c-4e51-b330-bfca6645a3b0|f29682d9-f1a4-4c73-96c6-b483743966e8|d7a39675-3c9b-419c-8a62-d7696fe9a8c4";
//    static  String json = "{\"accountId\":\"4e5e0301-a7a0-4fce-9492-493e37cd599c\",\"receiverName\":null,\"senderName\":\"ClipCall bot\",\"receiverIsPro\":false,\"data\":null,\"senderIsPro\":false,\"senderId\":\"00000000-0000-0000-0000-000000000000\",\"senderImageUrl\":\"https://app.clipcall.it/ccimages/shield-chat.png\",\"receiverId\":\"00000000-0000-0000-0000-000000000000\",\"receiverImageUrl\":null,\"receiverUsers\":[{\"userId\":\"86a52ac1-6fd8-4b1a-bbe4-032ac6ddf890\",\"isPro\":true},{\"userId\":\"f8c49dc4-c59b-4b06-8c3c-7d7b8fd7c1e8\",\"isPro\":false}],\"senderTitle\":{\"proTitle\":\"You've been booked!\",\"customerTitle\":\"You've successfully booked MarinaPro!\"},\"includeSms\":false}";
    static  String json = "{\"accountId\":\"4e5e0301-a7a0-4fce-9492-493e37cd599c\",\"receiverName\": \"ClipCall bot\",\"senderName\":\"ClipCall bot\"}"; //\"receiverIsPro\":false,\"data\":null,\"senderIsPro\":false,\"senderId\":\"00000000-0000-0000-0000-000000000000\",\"senderImageUrl\":\"https://app.clipcall.it/ccimages/shield-chat.png\",\"receiverId\":\"00000000-0000-0000-0000-000000000000\",\"receiverImageUrl\":null,\"receiverUsers\":[{\"userId\":\"86a52ac1-6fd8-4b1a-bbe4-032ac6ddf890\",\"isPro\":true},{\"userId\":\"f8c49dc4-c59b-4b06-8c3c-7d7b8fd7c1e8\",\"isPro\":false}],\"senderTitle\":{\"proTitle\":\"You've been booked!\",\"customerTitle\":\"You've successfully booked MarinaPro!\"},\"includeSms\":false}";

    static  String json1 = "{\"var1\":\"value\"}";
    public static void main(String[] args) {

//        parseStr(getJson(json1));
//        System.out.println( "is need to add : " + isNeedToAdd(sConversationKey, "f29682d9-f1a4-4c73-96c6-b483743966e8", false));
//        System.out.println( stupidParseAddress(adr));

        System.out.println("13%1 = " + 13%1 );
        System.out.println("13.01%1 = " + 13.01%1 );

    }

    private static String getJson(String json) {
        return json;
    }

    private static void parseStr(String json) {
        try {
            JSONObject jsonObj = new JSONObject(json);
            JSONArray receiverUsers = jsonObj.getJSONArray("receiverUsers");
            System.out.println(receiverUsers.toString());
//            for (int i = 0; i < receiverUsers.length(); i++) {
//                JSONObject c = receiverUsers.getJSONObject(i);
//
//                String id = c.getString("id");
//                String name = c.getString("name");
//                String email = c.getString("email");
//                String address = c.getString("address");
//                String gender = c.getString("gender");
//            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private static boolean isNeedToAdd(String sConversationKey, String customerId, boolean isInProMode) {

        if (sConversationKey == null || customerId == null){
            return false;
        }
        if (sConversationKey.isEmpty() || customerId.isEmpty()){
            return false;
        }

        if (!sConversationKey.contains(customerId)){
            return false;
        }
        String[] separated = sConversationKey.split("\\|");
        if (separated == null){
            return false;
        }
        if (separated.length == 3){
            if (isInProMode){
                if (customerId.equals(separated[2])){
                    return true;
                }
            }else {
                if (customerId.equals(separated[1])){
                    return true;
                }
            }
        }else {
            return false;
        }
        return false;
    }

    private static String stupidParseAddress(String address) {
        String result = "parse address error";
        String firstPart;
        String secondPart;
        String thirdPart;

        if (address.isEmpty()){
            return result;
        }

        String[] separated = address.split("0:\"");
        if (separated == null){
            return result;
        }
        if (separated.length > 1){
            result = separated[1];
            firstPart = result.substring(0, result.indexOf('\"'));
            result = firstPart;

        }

//        for (String item : separated)
//        {
//            System.out.println("item = " + item);
//        }



        return result;
    }
}
