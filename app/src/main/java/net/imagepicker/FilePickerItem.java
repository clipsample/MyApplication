package net.imagepicker;

import android.net.Uri;

/**
 * Created by Android on 01.02.2018.
 */

public class FilePickerItem {
    int position;
    Uri uri;

    public FilePickerItem(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }
}
