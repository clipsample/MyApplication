package net.imagepicker;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.android.myapplication.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.support.test.InstrumentationRegistry.getContext;

/**
 * Created by Android on 01.02.2018.
 */

public class MediaFilePickerAdapter extends RecyclerView.Adapter<MediaFilePickerAdapter.ViewHolder> {
    private  int width;
    private List<FilePickerItem> mValues;
    private final OnFilePickerActionsListener mListener;
    private  RecyclerView recyclerView;
    private int THUMBSIZE = 100;
    private Context context;

    public interface OnFilePickerActionsListener {
        void onAction(FilePickerItem item, OnFilePickedListener onFilePickedListener);
    }
    public interface OnFilePickedListener {
        void onFilesSelected(List<Uri> selectedFiles);
        void onFileSelected(Bitmap bitmap);
    }

    public MediaFilePickerAdapter(Context context, OnFilePickerActionsListener listener) {
        mValues = new ArrayList<>();
        addDefaultElemets();
        mListener = listener;
        this.context = context;

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (wm != null) {
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int w = size.x - (int)(2*(context.getResources().getDimension(R.dimen.activity_horizontal_margin)));
            width = (w / 3);
        }


    }

    private void addDefaultElemets() {
        for (int i = 0; i < 4; i++) {
            mValues.add(new FilePickerItem(i));
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.media_file_picker_item, parent, false);
        RecyclerView.LayoutParams opngoParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        opngoParams.width = width;
        if (width != 0) {
            view.setLayoutParams(opngoParams);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.tvDebug.setText("" + holder.mItem.getPosition());

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;

    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public final View mView;
        public final ImageView ivPicedFileImageView;
        public final ImageView ivRemoveFileImageView;
        public final ProgressBar pbFilePicking;
        public FilePickerItem mItem;

        public final TextView tvDebug;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ivPicedFileImageView = (ImageView) view.findViewById(R.id.iv_picked_file_preview);
            ivRemoveFileImageView = (ImageView) view.findViewById(R.id.iv_remove_picked_file);
            pbFilePicking = (ProgressBar) view.findViewById(R.id.pb_file_picking);

            tvDebug = (TextView) view.findViewById(R.id.tv_debug);

            mView.setOnClickListener(this);
            ivRemoveFileImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("TODEL", "------ ViewHolder.onClick: ivRemoveFileImageView = " + mItem.getPosition());
                    if (getItemCount() > 4) {
                        removeItem(mItem);
                    }else {
                        ivRemoveFileImageView.setVisibility(View.GONE);
                        removeFile(mItem);
                    }
                }
            });
        }


        @Override
        public void onClick(View view) {
            Log.e("TODEL", "------ ViewHolder.onClick: = " + mItem.getPosition());
            if (mListener != null) {
                ivRemoveFileImageView.setVisibility(View.VISIBLE);
                mListener.onAction(mItem, new OnFilePickedListener() {
                    @Override
                    public void onFilesSelected(List<Uri> selectedFiles) {
                        Log.e("TODEL", "------ ViewHolder.onFilesSelected: = ");
                            if (selectedFiles == null){
                                return;
                            }else if (selectedFiles.isEmpty()){
                                return;
                            }else if (selectedFiles.size() == 1){
                                String filePath = getRealPathFromURI (selectedFiles.get(0));
                                if (TextUtils.isEmpty(filePath)){
//                                    return;
                                }
                                Picasso.with(context).load(selectedFiles.get(0))
                                        .fit()
                                        .into(ivPicedFileImageView);
//                                ivPicedFileImageView.setImageBitmap(makeFileThumbnail(selectedFiles.get(0))); //TODO picasso
                                mItem.setUri(selectedFiles.get(0));
                            }else {
                                updateItemsList(selectedFiles);
                            }
                        Log.e("TODEL", "------ ViewHolder.onFilesSelected: selectedFiles.size() = " + selectedFiles.size());

                    }

                    @Override
                    public void onFileSelected(Bitmap bitmap) {
                        Log.e("TODEL", "------ ViewHolder.onFileSelected: = " + bitmap);
//                        ivPicedFileImageView.setImageBitmap(bitmap);

                    }
                });

                addEmptyItem();
            }
        }
    }

    private void updateItemsList(List<Uri> selectedFiles) {

    }

    private Bitmap makeFileThumbnail(Uri filePath) {
        Log.e("TODEL", "------ MediaFilePickerAdapter.makeFileThumbnail: = " + filePath);
        Bitmap thumbnail = null;
        if ("image".contains(filePath.toString())) {
            thumbnail = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(getRealPathFromURI(filePath)), THUMBSIZE, THUMBSIZE);

        } else  if ("video".contains(filePath.toString())) {
            Log.e("TODEL", "------ MediaFilePickerAdapter.makeFileThumbnail: getVideoPath(filePath) = " + getVideoPath(filePath));
//            getVideoPath(filePath)  ;
//            thumbnail = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Images.Thumbnails.MINI_KIND);
        }
//        Log.e("TODEL", "------ MediaFilePickerAdapter.makeFileThumbnail: = " + thumbnail.getByteCount());
        return thumbnail;
    }

    private String getRealPath(Uri contentUri) {
        File myFile = new File(contentUri.toString());
        Log.e("TODEL", "------ MediaFilePickerAdapter.getRealPath: = " + myFile.getAbsolutePath().toString());
        return "AAAAAAAAAAAAAAAAA";
    }
    private String getRealPathFromURI(Uri contentUri) {
        Log.e("TODEL", "------ MediaFilePickerAdapter.getRealPathFromURI: = " + contentUri.getClass().getSimpleName());
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        Log.e("TODEL", "------ MediaFilePickerAdapter.getRealPathFromURI: result = " + result);
        return result;
    }

    public String getVideoPath(Uri uri) {
        String[] projection = { MediaStore.Video.Media.DATA };
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }
//    public String getFileNameByUri(Uri uri) {
//        String fileName="unknown";//default fileName
//        Uri filePathUri = uri;
//        if (uri.getScheme().toString().compareTo("content")==0) {
//            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
//            if (cursor.moveToFirst()) {
//                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);//Instead of "MediaStore.Images.Media.DATA" can be used "_data"
//                filePathUri = Uri.parse(cursor.getString(column_index));
//                fileName = filePathUri.getLastPathSegment().toString();
//            }
//        } else if (uri.getScheme().compareTo("file")==0) {
//            fileName = filePathUri.getLastPathSegment().toString();
//        } else {
//            fileName = fileName+"_"+filePathUri.getLastPathSegment();
//        }
//        Log.e("TODEL", "------ MediaFilePickerAdapter.getFileNameByUri: = " + fileName);
//        return fileName;
//    }
//    private String getRealPathFromURI( Uri contentUri) {
//        Cursor cursor = null;
//        try {
//            String[] proj = { MediaStore.Images.Media.DATA };
//            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
//            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//            cursor.moveToFirst();
//            return cursor.getString(column_index);
//        } finally {
//            if (cursor != null) {
//                cursor.close();
//            }
//        }
//    }
    private void removeFile(FilePickerItem mItem) {

    }


    void addEmptyItem(){
        mValues.add(new FilePickerItem(getItemCount()));
        notifyDataSetChanged();
        recyclerView.scrollToPosition(getItemCount()-1);
    }

    void removeItem(FilePickerItem item){
        mValues.remove(item);
        notifyDataSetChanged();
        recyclerView.scrollToPosition(getItemCount()-1);
    }

    private void updatePositions(){
        for (int i = 0; i < mValues.size(); i++) {
            mValues.get(i).setPosition(i);
        }
    }
    public List<FilePickerItem> getmValues() {
        return mValues;
    }

    public void setmValues(List<FilePickerItem> mValues) {
        this.mValues = mValues;
    }
}
