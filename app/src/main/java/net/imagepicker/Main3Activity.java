package net.imagepicker;

import android.app.Activity;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.android.myapplication.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Main3Activity extends AppCompatActivity implements MediaFilePickerAdapter.OnFilePickerActionsListener {

    int SELECT_SINGLE_IMAGE = 111;
    int SELECT_SINGLE_VIDEO = 555;
    int REQUEST_CAMERA = 222;
    int SELECT_MULTIPLE_IMAGES = 333;
    int SELECT_MULTIPLE_VIDEOS = 444;
    String userChoosenTask ;
    MediaFilePickerAdapter.OnFilePickedListener onFilePickedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        RecyclerView rvContacts = (RecyclerView) findViewById(R.id.rvFiles);
        MediaFilePickerAdapter adapter = new MediaFilePickerAdapter(this, this);
        // Attach the adapter to the recyclerview to populate items
        rvContacts.setAdapter(adapter);
        // Set layout manager to position the items
        rvContacts.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        AlertDialog.Builder builder = new AlertDialog.Builder(Main3Activity.this);
        builder.setTitle("Add Photo!");
//        RecyclerView.ItemDecoration itemDecoration = new
//                DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL);
//        rvContacts.addItemDecoration(itemDecoration);
    }

    @Override
    public void onAction(FilePickerItem item, MediaFilePickerAdapter.OnFilePickedListener onFilePickedListener) {
        this.onFilePickedListener = onFilePickedListener;
        selectImage();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("TODEL", "------ Main3Activity.onActivityResult: resultCode = " + resultCode);
        Log.e("TODEL", "------ Main3Activity.onActivityResult: requestCode = " + requestCode);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_SINGLE_IMAGE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data); //TODO video
            else if (requestCode == SELECT_MULTIPLE_IMAGES)
                onSelectFromGalleryMultipleImages(data);
            else if (requestCode == SELECT_MULTIPLE_VIDEOS)
                onSelectFromGalleryMultypleVideos(data);
            else if (requestCode == SELECT_SINGLE_VIDEO)
                onSelectFromGalleryVideo(data);
        }
        this.onFilePickedListener = null;
    }

    private void onSelectFromGalleryMultypleVideos(Intent data) {

    }

    private void onSelectFromGalleryVideo(Intent data) {
        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
        if(data.getData()!=null){
            Uri mImageUri = data.getData();
            Log.w("TODEL", "------ Main3Activity.onSelectFromGalleryVideo: mImageUri = " + mImageUri);
            mArrayUri.add(mImageUri);
            if (onFilePickedListener != null) {
                onFilePickedListener.onFileSelected(onSelectFromGalleryResult(data));
                return;
            }

        }else{
            if(data.getClipData()!=null){
                ClipData mClipData=data.getClipData();
                for(int i=0;i<mClipData.getItemCount();i++){

                    ClipData.Item item = mClipData.getItemAt(i);
                    Uri uri = item.getUri();
                    mArrayUri.add(uri);

                }
                Log.v("LOG_TAG", "Selected Images"+ mArrayUri.size());
            }

        }

        for (Uri mImageUri :mArrayUri) {
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryVideo: ======================================= ");
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryVideo: = " + mImageUri.getEncodedPath());
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryVideo: = " + mImageUri.getPath());
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryVideo: = " + mImageUri.getLastPathSegment());
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryVideo: = " + mImageUri.getHost());
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryVideo: = " + mImageUri.getQuery());
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryVideo: = " + mImageUri.getEncodedQuery());
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryVideo: ---------------------------------------- " );
            for (String string:mImageUri.getPathSegments()) {
                Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryVideo: = " + string);
            }
        }
        if (onFilePickedListener != null) {
            onFilePickedListener.onFilesSelected(mArrayUri);
        }
    }

    private void onSelectFromGalleryMultipleImages(Intent data) {
        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
        if(data.getData()!=null){
            Uri mImageUri = data.getData();
            Log.w("TODEL", "------ Main3Activity.onSelectFromGalleryMultipleImages: mImageUri = " + mImageUri);
            mArrayUri.add(mImageUri);


        }else{
            if(data.getClipData()!=null){
                ClipData mClipData=data.getClipData();
                for(int i=0;i<mClipData.getItemCount();i++){

                    ClipData.Item item = mClipData.getItemAt(i);
                    Uri uri = item.getUri();
                    mArrayUri.add(uri);

                }
                Log.v("LOG_TAG", "Selected Images"+ mArrayUri.size());
            }

        }

        for (Uri mImageUri :mArrayUri) {
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryMultipleImages: ======================================= ");
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryMultipleImages: = " + mImageUri.getEncodedPath());
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryMultipleImages: = " + mImageUri.getPath());
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryMultipleImages: = " + mImageUri.getLastPathSegment());
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryMultipleImages: = " + mImageUri.getHost());
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryMultipleImages: = " + mImageUri.getQuery());
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryMultipleImages: = " + mImageUri.getEncodedQuery());
            Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryMultipleImages: ---------------------------------------- " );
            for (String string:mImageUri.getPathSegments()) {
                Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryMultipleImages: = " + string);
            }
        }
        if (onFilePickedListener != null) {
            onFilePickedListener.onFilesSelected(mArrayUri);
        }

    }

    @SuppressWarnings("deprecation")
    private Bitmap onSelectFromGalleryResult(Intent data) {
        Log.e("TODEL", "------ Main3Activity.onSelectFromGalleryResult: = ");
        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (onFilePickedListener != null) {
            onFilePickedListener.onFileSelected(bm);
        }
        return bm;
//        ivImage.setImageBitmap(bm);
    }


    private void onCaptureImageResult(Intent data) {
        Log.e("TODEL", "------ Main3Activity.onCaptureImageResult: = ");

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        ivImage.setImageBitmap(thumbnail);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take a photo or video"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose image from Library"))
                        galleryImageIntent();
                    else if(userChoosenTask.equals("Choose video from Library"))
                        galleryVideoIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }


    private void selectImage() {
        Log.w("TODEL", "------ Main3Activity.selectImage: = ");
        final CharSequence[] items = { "Take a photo or video", "Choose image from Library", "Choose video from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(Main3Activity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(Main3Activity.this);
                if (items[item].equals("Take a photo or video")) {
                    userChoosenTask="Take a photo or video";
                    if(result)
                        cameraIntent();
                } else if (items[item].equals("Choose image from Library")) {
                    userChoosenTask = "Choose image from Library";
                    if (result)
                        galleryImageIntent();
                }else if (items[item].equals("Choose video from Library")) {
                    userChoosenTask = "Choose video from Library";
                    if (result)
                        galleryVideoIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryImageIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), SELECT_MULTIPLE_IMAGES);
//        startActivityForResult(Intent.createChooser(intent,"Select Picture"), SELECT_SINGLE_IMAGE);
    }

    private void galleryVideoIntent() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);//
//        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_MULTIPLE_VIDEOS);
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_SINGLE_VIDEO);
    }
}
//https://stackoverflow.com/questions/4922037/android-let-user-pick-image-or-video-from-gallery

//(EDIT: I don't use it anymore, we went back to the two choices "pick image" and "pick video". The problem was with some Sony phone. So, it's not 100% solution below, be careful! )