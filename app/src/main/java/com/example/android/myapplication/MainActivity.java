package com.example.android.myapplication;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.android.myapplication.dummy.CreditCardDetailsActivity;
import com.example.android.myapplication.dummy.DummyContent;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements ItemFragment.OnListFragmentInteractionListener {

    private static final String TAG = "myLogs";
    String pattern;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        View decor = getWindow().getDecorView();
        decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        Button btn = (Button) findViewById(R.id.btn_click);
        Button btnStartAmazon = (Button) findViewById(R.id.btn_start_amazon);
        btnStartAmazon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AmazonUploadActivity.class);
                startActivity(intent);
            }
        });
        final TextView textView = (TextView)findViewById(R.id.tv_hello);

        final boolean[] btnFlag = {false};
        btn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, CreditCardDetailsActivity.class);
//                Intent intent = new Intent(MainActivity.this, LocationActivity.class);
//                Intent intent = new Intent(MainActivity.this, SharedPrefsActivity.class);
//                Intent intent = new Intent(MainActivity.this, AndroidCameraApi.class);
                Intent intent = new Intent(MainActivity.this, Camera1Activity.class);
                startActivity(intent);
//                if (btnFlag[0]){
//                    getWindow().setStatusBarColor(Color.TRANSPARENT);
//                    textView.setText("setStatusBarColor(Color.TRANSPARENT)");
//                }else {
//                    getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
//                    textView.setText("setStatusBarColor(getColor(R.color.colorPrimary)");
//                }
//                btnFlag[0] = !btnFlag[0];
            }
        });


        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String date = timeStampFormat.format(System.currentTimeMillis());
        long test = 1488457688984l;
//        long test = System.currentTimeMillis();
        String date = timeStampFormat.format(test);

        Log.e(TAG, "onCreate: date = " + date + " , test =  " + test);
        Date myDate = new Date();

        TimeZone gmtTime = TimeZone.getTimeZone("GMT");
        timeStampFormat.setTimeZone(gmtTime);

//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeZone(TimeZone.getTimeZone("GMT"));
//        calendar.setTime(myDate);
//        Date time = calendar.getTime();
//        SimpleDateFormat outputFmt = new SimpleDateFormat("MMM dd, yyy h:mm a zz");
//        String dateAsString = timeStampFormat.format(time);
        String dateAsString = timeStampFormat.format(myDate);

//        Log.e(TAG, "local date = " + date + "\n utc date = " + dateAsString);
//        String testStr = " aaa bbb ";
//        Log.e(TAG, "onCreate: --------------------------------------------------------------- testStr=" + testStr) ;
//        testStr = testStr.trim();
//        Log.e(TAG, "onCreate: --------------------------------------------------------------- testStr=" + testStr) ;
//
//        pattern = "^.+[a-zA-Z]\\s+.+[a-zA-Z]$";
////        pattern = "^[a-zA-Z ]+\\s+$";
//
//        String test1 = "aaa bbb";
//        String test2 = " aaa bbb ";
//        String test3 = "       aaa            bbb          ";
//        String test4 = "AAA";
//        String test5 = "AAA ";
//        String test6 = "AAA      ";
//        String test7 = " AAA      ";
//        String test8 = " AAA     AAA AAA ";
//        test(test1);
//        test(test2);
//        test(test3);
//        test(test4);
//        test(test5);
//        test(test6);
//        test(test7);
//        test(test8);
//
//        dateChecking();

//        showWarningDialog("Payment failed", "OK", "Please re-enter your credit card \n information or use another card");

//        Fragment fragment = ItemFragment.newInstance(1);
//        getSupportFragmentManager()
//                .beginTransaction().add(R.id.container, fragment, "fuck").commit();
        testingNewTread();
    }

    private void testingNewTread() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.e(TAG, "run: started" );
                for (int i = 0; i < 10; i++) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Log.e(TAG, "run: i = " + i);
                }
                Log.e(TAG, "run: finished");
            }
        }).start();

    }


    public boolean test(String testString){
        boolean res = false;
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(testString.trim());
        res = m.matches();
//        Log.e(TAG, "test: testString = " + testString + "  result = " + res);

//        if (res)
//        while (m.find()){
//            Log.e(TAG, "test: matcher.group(1)" + m.group(1) + " , + m.group(2) = " +  m.group(2));
//        }
        return res;
    }
    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {

    }

    void dateChecking(){
        String cardDD = "01";
        String cardMM = "02";
        String cardYY = "17";
        Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdfMonth = new SimpleDateFormat("MM/yyyy");
        Date cardExpDate = null;
        try {
            cardExpDate = sdf.parse("01/" + cardMM + "/20" + cardYY);
            Log.e("myLogs", "isExpirationLocalValid: " + cardExpDate + " =  " +  cardExpDate.getTime() );

            cardExpDate = sdf.parse("29/" + cardMM + "/20" + cardYY);
            Log.e("myLogs", "isExpirationLocalValid: "  + cardExpDate + " =  " +  cardExpDate.getTime() );

            cardExpDate = sdfMonth.parse( cardMM + "/20" + cardYY);
            Log.e("myLogs", "isExpirationLocalValid:  "  + cardExpDate + " =  " +  cardExpDate.getTime() );

            String newMonth = "" + Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH) + "/" + cardMM + "/20" + cardYY;
            cardExpDate = sdf.parse( newMonth);
            Log.e("myLogs", "isExpirationLocalValid:  " + cardExpDate + " =  " + newMonth + " = "+  cardExpDate.getTime() );

            cardExpDate = sdf.parse("03/" + cardMM + "/20" + cardYY);
            Log.e("myLogs", "isExpirationLocalValid: " + cardExpDate + " =  " +  cardExpDate.getTime() );

        } catch (ParseException e) {
            e.printStackTrace();
        }



        Date now = new Date();
        Log.e("myLogs", "isExpirationLocalValid: now = " + now + " " +  now.getTime() );
//        if (cardExpDate.after(now))

            Log.e("myLogs", "isExpirationLocalValid: cardExpDate.after(now) = " +  cardExpDate.after(now) );


    }

    private void showWarningDialog(String title, String buttonText, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        View view = (LinearLayout) getLayoutInflater()
                .inflate(R.layout.dialog_warning, null);
        alertDialogBuilder. setView(view);
        alertDialogBuilder.setCancelable(true);

        TextView titleView = (TextView)view.findViewById(R.id.dialog_warning_title);
        titleView.setText(title);

        TextView messageView = (TextView)view.findViewById(R.id.dialog_warning_message);
        messageView.setText(message);

        Button btnOk = (Button) view.findViewById(R.id.dialog_warning_btn_ok);
        btnOk.setText(buttonText);

        final AlertDialog alertDialog = alertDialogBuilder.create();
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();


//        if (titleView != null) {
//            titleView.setGravity(Gravity.CENTER);
//        }
//
//
//        alertDialogBuilder
//                .setTitle(title)
//                .setCancelable(true)
//                .setMessage(message)
//                .setPositiveButton(buttonText,
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog,
//                                                int id) {
//                                dialog.cancel();
//                            }
//                        });
//        AlertDialog alertDialog = alertDialogBuilder.create();
//        alertDialog.show();
//
//        final Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
//        positiveButton.setTextColor(getResources().getColor(R.color.link_color));
//        LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
//        positiveButtonLL.gravity = Gravity.CENTER;
//        positiveButton.setLayoutParams(positiveButtonLL);
//
//        TextView messageView = (TextView)alertDialog.findViewById(android.R.id.message);
//        messageView.setGravity(Gravity.CENTER);
//
//        TextView titleView = (TextView)alertDialog.findViewById(getResources().getIdentifier("alertTitle", "id", "android"));
//        if (titleView != null) {
//            titleView.setGravity(Gravity.CENTER);
//        }
    }
}
