package com.example.android.myapplication.dummy;

import android.graphics.Color;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.example.android.myapplication.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreditCardDetailsActivity extends AppCompatActivity {

    private static final String TAG = "myLogs";

    TextInputLayout tilFullName;
    EditText fullNameEditText;

    TextInputLayout tilEmail;
    EditText emailEditText;

    TextInputLayout tilCardNumber;
    EditText creditCardNumberEditText;

    TextInputLayout tilCardCVV;
    EditText cvvEditText;

    TextInputLayout tilMM;
    EditText creditCardExpirationMonthEditText;

    TextInputLayout tilYY;
    EditText creditCardExpirationYearEditText;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.credit_card_details_activity);

        fullNameEditText = (EditText)findViewById(R.id.fullNameEditText);
        tilFullName = (TextInputLayout) findViewById(R.id.tilfullName);

        emailEditText = (EditText)findViewById(R.id.emailEditText);
        tilEmail = (TextInputLayout) findViewById(R.id.tilEmail);

        tilCardNumber = (TextInputLayout) findViewById(R.id.tilCreditCardNumber);
        creditCardNumberEditText = (EditText)findViewById(R.id.creditCardNumberEditText);

        tilCardCVV = (TextInputLayout) findViewById(R.id.tilCvv);
        cvvEditText = (EditText)findViewById(R.id.cvvEditText);

        tilMM = (TextInputLayout) findViewById(R.id.expirationMonthContainer);
        creditCardExpirationMonthEditText = (EditText)findViewById(R.id.creditCardExpirationMonthEditText);

        tilYY = (TextInputLayout) findViewById(R.id.expirationYearContainer);
        creditCardExpirationYearEditText = (EditText)findViewById(R.id.creditCardExpirationYearEditText);

        setFullNameValidationTextChangedListener();
        setEmailValidationTextChangedListener();
        setCardNumberValidationTextChangedListener();
        setCardCVVValidationTextChangedListener();
        setCardExpirationValidationTextChangedListener();



    }

    private void setCardExpirationValidationTextChangedListener() {
        if (creditCardExpirationMonthEditText == null || creditCardExpirationYearEditText == null)
            return;
        creditCardExpirationMonthEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                boolean isExpValid = isCardExpirationDateValidLocal(creditCardExpirationMonthEditText.getText().toString(), creditCardExpirationYearEditText.getText().toString());
                fieldValidation(tilMM, creditCardExpirationMonthEditText, isExpValid);
                fieldValidation(tilYY, creditCardExpirationYearEditText, isExpValid);
            }
        });

        creditCardExpirationYearEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                boolean isExpValid = isCardExpirationDateValidLocal(creditCardExpirationMonthEditText.getText().toString(), creditCardExpirationYearEditText.getText().toString());
                fieldValidation(tilMM, creditCardExpirationMonthEditText, isExpValid);
                fieldValidation(tilYY, creditCardExpirationYearEditText, isExpValid);
            }
        });


    }

    private void setCardCVVValidationTextChangedListener() {
        if (cvvEditText == null)
            return;
        cvvEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                fieldValidation(tilCardCVV, cvvEditText, isCardCVVValidLocal(cvvEditText.getText().toString()));
            }
        });
    }

    private void setCardNumberValidationTextChangedListener() {
        if (creditCardNumberEditText == null)
            return;
        creditCardNumberEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                fieldValidation(tilCardNumber, creditCardNumberEditText, isCardNumberValidLocal(creditCardNumberEditText.getText().toString()));
            }
        });
    }

    private void setFullNameValidationTextChangedListener(){
        if (fullNameEditText == null)
            return;
        fullNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                fieldValidation(tilFullName, fullNameEditText, isFullNameValidLocal(fullNameEditText.getText().toString()));
            }
        });
    }

    private void setEmailValidationTextChangedListener(){
        if (emailEditText == null)
            return;
        emailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                fieldValidation(tilEmail, emailEditText, isValidEmailLocal(emailEditText.getText().toString()));
            }
        });
    }

    private void fieldValidation (TextInputLayout til, EditText etFullName, boolean isValid) {
        if (til == null || etFullName == null)
            return;
        if (isValid){
            til.setErrorEnabled(false);
            etFullName.setTextColor(Color.BLACK);
        }else {
            til.setErrorEnabled(true);
            til.setError(" ");
            etFullName.setTextColor(Color.RED);
        }
    }

    public boolean isFullNameValidLocal(String testString){
        if (testString == null)
            return false;
        boolean res = false;
        Pattern p = Pattern.compile("^\\s*.+[a-zA-Z]\\s+.+[a-zA-Z]\\s*$");
        Matcher m = p.matcher(testString);
        res = m.matches();
        return res;
    }

    public  boolean isValidEmailLocal(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public boolean isCardNumberValidLocal(String cardNumber) {
        if (cardNumber == null)
            return false;
        if (cardNumber.length() == 14 || cardNumber.length() == 16)
            return true;
        return false;
    }

    public boolean isCardCVVValidLocal(String cardCVV) {
        if (cardCVV == null)
            return false;
        if (cardCVV.length() == 3 || cardCVV.length() == 4)
            return true;
        return false;
    }

    public boolean isCardExpirationDateValidLocal(String cardMM, String cardYY) {
        if (cardMM == null || cardYY == null)
            return false;
        if (cardMM.isEmpty() || cardYY.isEmpty())
            return false;
        int month = Integer.parseInt(cardMM);
        int year = Integer.parseInt(cardYY);

        if (month < 10)
            cardMM = String.format("%02d", month);

        if (month < 1 || month > 12 || year < 16)
            return false;

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date cardExpDate = null;
        try {
            cardExpDate = sdf.parse("01/" + cardMM + "/20" + cardYY);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }

        Date now = new Date();

        if (cardExpDate.after(now))
                        return true;
        return false;
    }
}
