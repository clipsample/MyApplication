package com.example.android.myapplication.listener;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.android.myapplication.R;

public class ListenerActivity extends AppCompatActivity {

    static int counter = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("TODEL", "------ ListenerActivity.onCreate: = ");
        setContentView(R.layout.activity_listener);

        SomeActionListener someListener = new SomeActionListener() {
            @Override
            public void onAction() {
                Log.e("TODEL", "------ ListenerActivity.onAction: counter = " + counter);
                counter++;
            }
        };
//        ListenerDispatcher.addListener(someListener);
        SomeActionListener someListener2 = new SomeActionListener() {
            @Override
            public void onAction() {
                Log.e("TODEL", "------ ListenerActivity.onAction: 2222 counter = " + counter);
            }
        };
        someActionGenerator();
    }

    private void someActionGenerator() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    Log.e("TODEL", "------ ListenerActivity.run: = ");
                        ListenerDispatcher.notifyAllListebers();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
