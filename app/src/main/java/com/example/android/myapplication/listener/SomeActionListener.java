package com.example.android.myapplication.listener;

/**
 * Created by Android on 23.10.2017.
 */

public abstract class SomeActionListener {
    public SomeActionListener() {
        ListenerDispatcher.addListener(this);
    }
    abstract void onAction();
}
