package com.example.android.myapplication.listener;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Android on 23.10.2017.
 */

public class ListenerDispatcher {
    private static List<SomeActionListener> list = new ArrayList<>();
    public static void addListener( SomeActionListener someListener){
        Log.e("TODEL", "------ ListenerDispatcher.addListener: = ");
        list.add(someListener);
    }
    public static void notifyAllListebers(){
        Log.e("TODEL", "------ ListenerDispatcher.notifyAllListebers: list.size() = " + list.size());
        for (SomeActionListener someListener: list) {
            someListener.onAction();
        }
    }
}
