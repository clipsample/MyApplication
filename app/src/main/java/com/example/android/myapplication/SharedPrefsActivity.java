package com.example.android.myapplication;

import android.content.SharedPreferences;
import android.support.test.espresso.core.deps.guava.base.Strings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;

import java.util.HashSet;
import java.util.Set;

public class SharedPrefsActivity extends AppCompatActivity {

    private static final String KEY_SET_ARRIVED_LEAD_ID = "KEY_SET_ARRIVED_LEAD_ID";
    private static final String KEY_CLIP_CALL_APP_PREFS = "KEY_CLIP_CALL_APP_PREFS";
    private static final String TAG = "myLogs";

    Button btnAdd;
    Button btnRemove;
    ImageView ivTestSVG;
    private static int counter = 0;
    private static int removeCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_prefs);

        btnAdd = (Button)findViewById(R.id.btn_add_pref);
        btnRemove = (Button)findViewById(R.id.btn_remove_pref);
        ivTestSVG = (ImageView) findViewById(R.id.iv_test_svg);

        SVG svg = SVGParser.getSVGFromResource(getResources(), R.raw.ic_test_svg);
//        SVG svg = SVGParser.getSVGFromResource(getResources(), R.raw.ic_svg_test1);

//The following is needed because of image accelaration in some devices such as samsung
//        ivTestSVG.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        ivTestSVG.setImageDrawable(svg.createPictureDrawable());


btnAdd.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        addString();
    }
});

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeString();
            }
        });
    }

    private void removeString() {
        removeSetArrivedLeadId("string" + removeCounter);
        removeCounter++;
    }

    private void addString() {
        saveSetArrivedLeadId("string" + counter);
        counter++;
    }

    private void saveSetArrivedLeadId(String arrivedLeadId) {
        Log.e(TAG, "saveSetArrivedLeadId: " +  arrivedLeadId);
        if (!Strings.isNullOrEmpty(arrivedLeadId)) {
            Set<String> set = loadSetArrivedLeadIds();
            if (set != null && !set.isEmpty()) {
                if (!set.contains(arrivedLeadId)) {
                    set.add(arrivedLeadId);
                    SharedPreferences preferences = getSharedPreferences(KEY_CLIP_CALL_APP_PREFS, MODE_PRIVATE);
                    SharedPreferences.Editor ed = preferences.edit();
                    ed.putStringSet(KEY_SET_ARRIVED_LEAD_ID, set);
                    ed.commit();
                }
            }else {
                set = new HashSet<>();
                set.add(arrivedLeadId);
                SharedPreferences preferences = getSharedPreferences(KEY_CLIP_CALL_APP_PREFS, MODE_PRIVATE);
                SharedPreferences.Editor ed = preferences.edit();
                ed.putStringSet(KEY_SET_ARRIVED_LEAD_ID, set);
                ed.commit();
            }
        }
        Log.e(TAG, "saveSetArrivedLeadId: after removing " + arrivedLeadId + " set = ");
        loadSetArrivedLeadIds();
    }
    private void removeSetArrivedLeadId(String arrivedLeadId) {
        Log.e(TAG, "removeSetArrivedLeadId: " + arrivedLeadId );
        Set<String> set = loadSetArrivedLeadIds();
        if (set != null && !set.isEmpty()) {
            if (set.contains(arrivedLeadId)) {
                set.remove(arrivedLeadId);
                SharedPreferences preferences = getSharedPreferences(KEY_CLIP_CALL_APP_PREFS, MODE_PRIVATE);
                SharedPreferences.Editor ed = preferences.edit();
                ed.putStringSet(KEY_SET_ARRIVED_LEAD_ID, set);
                ed.commit();
            }
        }
        Log.e(TAG, "removeSetArrivedLeadId: after removing " + arrivedLeadId + " set = ");
        loadSetArrivedLeadIds();
    }
    private Set<String> loadSetArrivedLeadIds() {
        SharedPreferences preferences = getSharedPreferences(KEY_CLIP_CALL_APP_PREFS, MODE_PRIVATE);
        Set<String> set = preferences.getStringSet(KEY_SET_ARRIVED_LEAD_ID, null);

        if (set != null && !set.isEmpty()){
            Log.e(TAG, "loadSetArrivedLeadIds: set = " + set.toString());
        }else {
            Log.e(TAG, "loadSetArrivedLeadIds: empty ");
        }
//        set =
//        return set;
        return set;
    }
}
