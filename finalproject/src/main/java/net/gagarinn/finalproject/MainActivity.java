package net.gagarinn.finalproject;

import android.content.Intent;
import android.location.Location;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;


import net.gagarinn.finalproject.fragments.BaseFragment;
import net.gagarinn.finalproject.fragments.MapFragment;
import net.gagarinn.finalproject.fragments.RestorantsFragment;

public class MainActivity extends AppCompatActivity implements BaseFragment.OnFragmentInteractionListener {

    private final String TAG = this.getClass().getSimpleName();
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;


    private FragmentTabHost mTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }
    private void initViews() {

        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent );

        mTabHost.addTab(mTabHost.newTabSpec("MAP").setIndicator("MAP"), MapFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("List").setIndicator("List"), RestorantsFragment.class, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemId = item.getItemId();
        if (menuItemId == R.id.search){

            try {
                Intent intent =
                        new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                .build(this);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException e) {
                // TODO: Handle the error.
            } catch (GooglePlayServicesNotAvailableException e) {
                // TODO: Handle the error.
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i(TAG, "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Toast.makeText(this, getString(R.string.place_error_message), Toast.LENGTH_SHORT).show();
                Log.w(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onUseCurrentLocationTapped(Location location) {

    }

//   https://stackoverflow.com/questions/31136527/add-search-toolbar-over-google-map-like-in-native-android-app
//    https://developers.google.com/places/android-api/autocomplete?hl=ru#add_an_autocomplete_widget //todo

//    https://habrahabr.ru/post/256643/

//    https://www.journaldev.com/12478/android-searchview-example-tutorial
//    https://github.com/googlesamples/android-play-places/blob/master/PlacePicker/Application/build.gradle
//    https://developers.google.com/places/android-api/start?hl=ru
//    https://www.programcreek.com/java-api-examples/index.php?api=android.support.v7.widget.SearchView.OnQueryTextListener
//    https://stackoverflow.com/questions/21585326/implementing-searchview-in-action-bar
}
