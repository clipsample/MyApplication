package net.gagarinn.finalproject.fragments;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.gagarinn.finalproject.R;
import net.gagarinn.finalproject.fragments.dummy.DummyContent;
import net.gagarinn.finalproject.fragments.dummy.DummyContent.DummyItem;

import java.util.List;

public class RestorantsFragment extends BaseFragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;

    public RestorantsFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static RestorantsFragment newInstance(int columnCount) {
        RestorantsFragment fragment = new RestorantsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restorantitem_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
//            recyclerView.setAdapter(new RestorantItemRecyclerViewAdapter(DummyContent.ITEMS, mListener));
            recyclerView.setAdapter(new RestorantItemRecyclerViewAdapter(DummyContent.ITEMS, null));
        }
        return view;
    }

    @Override
    public void onButtonPressed(Location location) {
        Log.e("TAG", "------RestorantsFragment : onButtonPressed: ");
    }
    
    public interface OnListFragmentInteractionListener{

        void onListFragmentInteraction(DummyItem mItem);
    }
}
